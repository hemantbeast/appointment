﻿using System;
using System.Threading.Tasks;
using Appointment.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Appointment
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			MainPage = new NavigationPage(new MainPage()) {
				BarBackgroundColor = Color.FromHex("2196F3"),
				BarTextColor = Color.White
			};
		}

		protected override void OnStart()
		{
		}

		protected override void OnSleep()
		{
		}

		protected override void OnResume()
		{
		}

		public static async Task PushModalAsync(Page page)
		{
			await Current.MainPage.Navigation.PushModalAsync(new NavigationPage(page) {
				BarBackgroundColor = Color.FromHex("2196F3"),
				BarTextColor = Color.White
			});
		}

		public static async Task DisplayAlert(string mesage)
		{
			await Current.MainPage.DisplayAlert(string.Empty, mesage, "OK");
		}
	}
}
