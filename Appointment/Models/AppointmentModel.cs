﻿using System;
using System.Windows.Input;
using Appointment.ViewModels;
using Xamarin.Forms;

namespace Appointment.Models
{
	public class AppointmentModel : BaseNotifyPropertyChanged
	{
		bool _isSelected;
		string _title, _description;
		DateTime _date;

		public ICommand SelectedCommand { get; private set; }

		public AppointmentModel()
		{
			SelectedCommand = new Command(() => {
				IsSelected = !IsSelected;
			});
		}

		public bool IsSelected {
			get => _isSelected;
			set {
				_isSelected = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(SelectedImage));
			}
		}

		public string Title {
			get => _title;
			set {
				_title = value;
				OnPropertyChanged();
			}
		}

		public string Description {
			get => _description;
			set {
				_description = value;
				OnPropertyChanged();
			}
		}

		public DateTime Date {
			get => _date;
			set {
				_date = value;
				OnPropertyChanged();
			}
		}

		public string SelectedImage => IsSelected ? "ic_check" : "ic_uncheck";
	}
}
