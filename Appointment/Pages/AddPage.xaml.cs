﻿using System;
using Appointment.Models;
using Xamarin.Forms;

namespace Appointment.Pages
{
	public partial class AddPage : ContentPage
	{
		public Action<AppointmentModel> Added { get; set; }

		public AddPage(DateTime selectedDate)
		{
			InitializeComponent();

			datePicker.MinimumDate = new DateTime(1970, 1, 1);
			datePicker.MaximumDate = new DateTime(2030, 1, 1);

			datePicker.Date = selectedDate.Date;
			timePicker.Time = DateTime.Now.TimeOfDay;
		}

		void ToolbarItem_Clicked(object sender, EventArgs e)
		{
			Navigation.PopModalAsync();
		}

		async void Done_Clicked(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(title.Text)) {
				await App.DisplayAlert("Please enter title");
				return;
			}

			if (string.IsNullOrWhiteSpace(description.Text)) {
				await App.DisplayAlert("Please enter description");
				return;
			}

			Added?.Invoke(new AppointmentModel {
				Title = title.Text,
				Description = description.Text,
				Date = datePicker.Date.Add(timePicker.Time)
			});

			await Navigation.PopModalAsync();
		}
	}
}
