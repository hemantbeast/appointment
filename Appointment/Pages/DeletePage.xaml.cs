﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Appointment.Models;
using Appointment.ViewModels;
using Xamarin.Forms;

namespace Appointment.Pages
{
	public partial class DeletePage : ContentPage
	{
		DeleteAppointmentViewModel viewModel;

		public Action<ObservableCollection<AppointmentModel>> Deleted { get; set; }

		public DeletePage(ObservableCollection<AppointmentModel> appointmentList)
		{
			InitializeComponent();
			BindingContext = viewModel = new DeleteAppointmentViewModel(appointmentList);
		}

		void ToolbarItem_Clicked(object sender, EventArgs e)
		{
			Navigation.PopModalAsync();
		}

		void Delete_Clicked(object sender, EventArgs e)
		{
			var selectedList = viewModel.Appointments.Where(x => x.IsSelected).ToList();

			foreach (var item in selectedList) {
				viewModel.Appointments.Remove(item);
			}

			Deleted?.Invoke(viewModel.Appointments);
			Navigation.PopModalAsync();
		}
	}
}
