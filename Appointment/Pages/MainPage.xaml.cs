﻿using System;
using Xamarin.Forms;

namespace Appointment.Pages
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();

			calendar.MinimumDate = new DateTime(1970, 1, 1);
			calendar.MaximumDate = new DateTime(2030, 1, 1);
		}
	}
}
