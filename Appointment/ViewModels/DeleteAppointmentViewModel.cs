﻿using System;
using System.Collections.ObjectModel;
using Appointment.Models;

namespace Appointment.ViewModels
{
	public class DeleteAppointmentViewModel : BaseNotifyPropertyChanged
	{
		ObservableCollection<AppointmentModel> _appointments;

		public DeleteAppointmentViewModel(ObservableCollection<AppointmentModel> appointments)
		{
			Appointments = appointments;
		}

		public ObservableCollection<AppointmentModel> Appointments {
			get => _appointments;
			set {
				_appointments = value;
				OnPropertyChanged();
			}
		}
	}
}
