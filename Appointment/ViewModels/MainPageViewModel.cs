﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Appointment.Models;
using Appointment.Pages;
using Xamarin.Forms;
using Xamarin.Plugin.Calendar.Models;

namespace Appointment.ViewModels
{
	public class MainPageViewModel : BaseNotifyPropertyChanged
	{
		DateTime _selectedDate;
		EventCollection _events;

		public ICommand AppointmentAddCommand { get; private set; }
		public ICommand AppointmentDeleteCommand { get; private set; }

		public MainPageViewModel()
		{
			SelectedDate = DateTime.Now;
			Events = new EventCollection();

			AppointmentAddCommand = new Command(TapOnAdd);
			AppointmentDeleteCommand = new Command(TapOnDelete);
		}

		private async void TapOnAdd(object obj)
		{
			var page = new AddPage(SelectedDate);

			page.Added += (appointment) => {
				if (Events.ContainsKey(appointment.Date)) {
					var events = Events[appointment.Date] as ObservableCollection<AppointmentModel>;
					events.Add(appointment);
				} else {
					Events.Add(appointment.Date, new ObservableCollection<AppointmentModel> { appointment });
				}
			};

			await App.PushModalAsync(page);
		}

		private async void TapOnDelete(object obj)
		{
			if (!Events.Any()) {
				return;
			}

			if (Events[SelectedDate].Count == 1) {
				Events.Remove(SelectedDate);

			} else {
				var events = Events[SelectedDate] as ObservableCollection<AppointmentModel>;
				var page = new DeletePage(events);

				page.Deleted += (appointments) => {
					if (appointments.Count == 0) {
						Events.Remove(SelectedDate);
					} else {
						Events[SelectedDate] = appointments;
					}
				};

				await App.PushModalAsync(page);
			}
		}

		public DateTime SelectedDate {
			get => _selectedDate;
			set {
				_selectedDate = value;
				OnPropertyChanged();
			}
		}

		public EventCollection Events {
			get => _events;
			set {
				_events = value;
				OnPropertyChanged();
			}
		}
	}
}
